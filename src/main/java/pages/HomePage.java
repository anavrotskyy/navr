package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import elements.LoginContainerElement;
import elements.MainMenuElement;
import elements.NonExistingBlock;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;

public class HomePage extends BasePage<HomePage> {

    private final static String url = "http://www.buckknives.com/";
    private String productName;

    private MainMenuElement mainMenuElement;
    private LoginContainerElement loginContainerElement;
    private NonExistingBlock nonExistingBlock;
    private String username;
    private String password;

    @FindBy(id = "modal-email-signup-close")
    private Button closeEmailSubscribePopup;

    @FindBy(id = "modal-email-signup")
    private Button modalSignup;

    @FindBy(id = "modal-login")
    private Link loginModal;

    public HomePage(WebDriver driver) {
        super(driver);
        setUrl(url);
    }

    public SearchResultsPage searchForKnife(String knife) {
        waitForElementToBeClickable(mainMenuElement.searchField);
        mainMenuElement.serchFor(knife);
        return new SearchResultsPage(driver);
    }

    public HomePage loginApplication() {
        mainMenuElement.setOpenLoginModal();
        waitForElementToLackAttribute(loginModal, "style", "display: none;");
        waitForEelementToBeDisplayed(loginContainerElement.login);
        loginContainerElement.doLogin(username, password);
        waitForElementToLackAttribute(loginModal, "style", "display: block;");
        return this;
    }

    public String getTagFromExistingelement() {
        return waitForEelementToBeDisplayed(mainMenuElement.openLoginModal).getTagName();
    }

    public void existingelement() {
        waitForEelementToBePresent(loginModal);
    }

    public String getTagFromNONExistingelement() {
        return waitForEelementToBeDisplayed(driver.findElement(By.id("tested-here"))).getTagName();
    }

    public void nONExistingelement() {
        waitForEelementToBePresent(driver.findElement(By.id("tested-here")));
    }

    public String getTagFromExistingBlock() {
        return waitForEelementToBeDisplayed(mainMenuElement).getTagName();
    }

    public void existingBlock() {
        waitForEelementToBePresent(mainMenuElement);
    }

    public String getTagFromNONExistingBlock() {
        return waitForEelementToBeDisplayed(nonExistingBlock).getTagName();
    }

    public void nONExistingBlock() {
        long start = System.currentTimeMillis();
        try {
            nonExistingBlock.isDisplayed();
        } catch (Exception e) {
        } finally {
            System.out.println(System.currentTimeMillis() - start);
        }
    }

    public void getTagFromNONExistingElementInBlock(){
            long start = System.currentTimeMillis();
        try {
            mainMenuElement.nonExisting.isDisplayed();
        } catch (Exception e) {
        } finally {
            System.out.println(System.currentTimeMillis() - start);
        }
    }

    public void nONExistingElementInBlock() {
        waitForEelementToBePresent(mainMenuElement.nonExisting);
    }

    @Override
    protected void load() {
        driver.get(getUrl());
        if (closeEmailSubscribePopup.isDisplayed()) {
            closeEmailSubscribePopup.click();
        }
        waitForElementToLackAttribute(modalSignup, "style", "display: block;");
        //waitForEelementToBeDisplayed(mainMenuElement.mainLogo);
        //waitForElementToBeClickable(mainMenuElement.openLoginModal);
    }

    @Override
    protected void isLoaded() throws Error {
        try {
            Assert.assertTrue(!modalSignup.isDisplayed(), "main logo is not visible");
        } catch (Exception e) {
            throw new Error(e.getMessage());
        }
    }
}
