package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import elements.SearchResultsElements;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;

public class SearchResultsPage extends BasePage<SearchResultsPage> {

    private String productName;

    @FindBy(id = "product-display")
    public SearchResultsElements searchResultsElements;

    @FindBy(id = "sort-select")
    private Select sortResults;

    @FindBy(xpath = "//div[@class='product-count']/span")
    private WebElement productCount;

    @FindBy(id = "pdp-btn-add")
    private Button addToCart;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public ProductDetailsPage selectProduct(String prodName) {
        if (searchResultsElements.clickOnSearchResultElement(prodName)) {
            return new ProductDetailsPage(driver);
        } else {
            System.out.println("product not found");
            return null;
        }
    }

    public int productCount() {
        return Integer.parseInt(productCount.getText());
    }

    @Override
    protected void load() {
        throw new UnsupportedOperationException("This page should not be loaded directly");
    }

    @Override
    protected void isLoaded() {
        try {
            Assert.assertTrue(waitForEelementToBeDisplayed(addToCart).isDisplayed(), "sort results dropdown is not " +
                    "visible");
        } catch (Exception e) {
            throw new Error(e.getMessage());
        }
    }
}
