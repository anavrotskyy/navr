package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;

/**
 * Created by andriynavrotskyy on 12/14/16.
 */
public class ProductDetailsPage extends BasePage<ProductDetailsPage> {

    @FindBy(tagName = "h1")
    private TextBlock productName;

    @FindBy(id = "pdp-btn-add")
    private Button addToCart;

    @FindBy(id = "pdp-cart-success")
    private WebElement cartSuccessModal;

    @FindBy(css = "a.btn.btn-primary")
    private Link checkoutNow;

    public ProductDetailsPage(WebDriver driver) {
        super(driver);
    }

    public String getProductName(){
        return productName.getText();
    }

    public void addToCart(){
        addToCart.click();
        waitForEelementToBeDisplayed(cartSuccessModal);
    }

    public void openCartFrommodal(){
        checkoutNow.click();
    }

    @Override
    protected void load(){
    }

    @Override
    protected void isLoaded(){
        try {
            Assert.assertTrue(addToCart.isDisplayed(), "add to cart button is not visible");
        } catch (Exception e) {
            throw new Error(e.getMessage());
        }
    }
}
