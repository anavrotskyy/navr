package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

public abstract class BasePage<T extends LoadableComponent<T>> extends LoadableComponent<T> {

    protected WebDriver driver;
    private String url;

    protected void setUrl(String url) {
        this.url = url;
    }

    protected String getUrl() {
        return url;
    }

    public BasePage(WebDriver driver) {
        PageFactory.initElements(new EditedDecorator(new EditedHtmlElementLocatorFactory(driver)), this);
        this.driver = driver;
    }

    public <S extends WebElement> S waitForEelementToBeDisplayed(S element) {
        WebDriverWait wait = new WebDriverWait(driver, 2);
        S elemental = (S) wait.until(ExpectedConditions.visibilityOf(element));
        return elemental;
    }

    public <S extends WebElement> void waitForEelementToBePresent(S element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(presenceOfElement(element));
    }

    public void click(TypifiedElement button) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", button);
    }

    public <S extends WebElement> void waitForElementToBeClickable(S element) {
        FluentWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public <S extends WebElement> void waitForElementToLackAttribute(final S element, final String attribute,
                                                                     final String attributeValue) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return !element.getAttribute(attribute).contains(attributeValue);
            }
        });
    }

    public ExpectedCondition<Boolean> presenceOfElement(final WebElement element) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                try {
                    // just to check if could interact with element, if yes - it is present
                    element.getTagName();
                    return true;
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    return false;
                }
            }

            public String toString() {
                return "presence of element";
            }
        };
    }

    @Override
    protected void load() {
        driver.get(url);
    }

    @Override
    protected void isLoaded() throws Error {
        try {
            if (!new URL(this.url).getFile().equals(new URL(driver.getCurrentUrl()).getFile())) {
                throw new Error("Current URL should be: " + this.url + " but is actually " + driver.getCurrentUrl());
            }
        } catch (MalformedURLException e) {
            throw new Error(e);
        }
    }
}
