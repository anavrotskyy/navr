package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import elements.CheckoutAddressElements;
import elements.CheckoutPromoFormElements;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by andriynavrotskyy on 12/14/16.
 */
public class CheckoutPage extends BasePage<CheckoutPage> {

    private CheckoutAddressElements checkoutAddressElements;
    private CheckoutPromoFormElements checkoutPromoFormElements;

    @FindBy(id = "promocodefield")
    private TextInput promoCodeInput;

    @FindBy(id = "cartpromobtn")
    private Button applyPromoCode;

    @FindBy(css = "strong.subtotal")
    private TextBlock cartSubtotal;

    @FindBy(id = "editAddress")
    private Button editAddress;

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public int countExpectedSubtotal(int quantity){
        return quantity*checkoutPromoFormElements.getPrice();
    }

    public int getActualSubtotal(){
        return Integer.parseInt(cartSubtotal.getText().trim().replace("$","").replace(",","").split("\\.")[0]);
    }

    public void setItemsQuantity(int i){
        checkoutPromoFormElements.setQuantity(i);
    }



}
