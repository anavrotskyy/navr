package elements;

import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Link;

@Name("knife shop elements")
@FindBy(css = "a.subnav-link[data-ga-label='Knives']")
public class KnifeShopElements extends MainMenuElement {

    @FindBy(partialLinkText = "Hunting")
    private Link huntingKnives;

    @FindBy(partialLinkText = "Survival")
    private Link survivalKnives;

    @FindBy(partialLinkText = "Tactical")
    private Link tacticalKnives;

    @FindBy(partialLinkText = "Cutlery")
    private Link cutleryKnives;

    @FindBy(partialLinkText = "Fishing")
    private Link fishingKnives;

    @FindBy(partialLinkText = "Recreation")
    private Link recreationKnives;

    @FindBy(partialLinkText = "Everyday")
    private Link everydayKnives;

    @FindBy(partialLinkText = "Legacy")
    private Link legacyKnives;
}
