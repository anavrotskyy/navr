package elements;

import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextInput;

@Name("login modal container")
@FindBy(id = "login-container")
public class LoginContainerElement extends HtmlElement{

    @FindBy(id = "input-username")
    private TextInput username;

    @FindBy(id = "input-password")
    private TextInput password;

    @FindBy(id = "form-login-btn")
    public Button login;

    public void doLogin(String email, String pass){
        username.click();
        username.clear();
        username.sendKeys(email);
        password.clear();
        password.sendKeys(pass);
        login.submit();
    }

}
