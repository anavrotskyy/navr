package elements;

import org.apache.xalan.xsltc.util.IntegerArray;
import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by andriynavrotskyy on 12/22/16.
 */

@Name("items in your cart panel")
@FindBy(id = "orderItemForm")
public class CheckoutPromoFormElements extends HtmlElement {

    @FindBy(id = "cartqtyinput")
    private TextInput itemsQuantity;

    @FindBy(xpath = ".//a[contains(text(), 'Update')]")
    private Link updateCart;

    @FindBy(xpath = ".//a[contains(text(), 'Remove')]")
    private Link removeFromCart;

    @FindBy(xpath = ".//div[@class='cart-skudims']/p")
    private TextBlock price;

    public void setQuantity(Integer quantity){
        itemsQuantity.clear();
        itemsQuantity.sendKeys(quantity.toString());
        updateCart.click();
    }

    public int getPrice(){
        return Integer.parseInt(price.getText().replace("Price","").trim().replace("$","").replace(",","").split("\\.")[0]);
    }
}
