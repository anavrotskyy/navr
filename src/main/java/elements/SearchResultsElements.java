package elements;

import org.openqa.selenium.support.FindBy;

import java.util.List;

import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Link;

public class SearchResultsElements extends HtmlElement {

    @FindBy(xpath = ".//a[starts-with(@id, 'product-thumb-')]")
    List<Link> productList;

    public boolean isProductFound(String searchQuery) {
        for (Link link : productList) {
            if (productList != null && link.getAttribute("data-ga-name").contains(searchQuery)) {
                return true;
            }
        }
        return false;
    }

    public boolean clickOnSearchResultElement(String searchedElement) {
        for (Link link : productList) {
            if (productList != null && link.getAttribute("data-ga-name").contains(searchedElement)) {
                link.click();
                return true;
            }
        }
        return false;
    }

    public int searchResults() {
        return productList.size();
    }


}
