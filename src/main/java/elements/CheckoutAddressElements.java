package elements;

import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by andriynavrotskyy on 12/22/16.
 */
@Name("User Information Form")
@FindBy(id = "checkoutAddressForm")
public class CheckoutAddressElements extends HtmlElement {

    @FindBy(id = "billing_email")
    private TextInput billingEmail;

    @FindBy(id = "billing_firstName")
    private TextInput billingFirstName;

    @FindBy(id = "billing_lastName")
    private TextInput billingLastName;

    @FindBy(id = "billing_address1")
    private TextInput billingAddress1;

    @FindBy(id = "billing_address2")
    private TextInput billingAddress2;

    @FindBy(id = "billing_city")
    private TextInput billingCity;

    @FindBy(id = "billing_state")
    private Select billingStateProvince;

    @FindBy(id = "billing_postalCode")
    private TextInput billingPostalCode;

    @FindBy(id = "billing_country")
    private Select billingCountry;

    @FindBy(id = "billing_phone")
    private TextInput billingPhone;

    @FindBy(id = "useBillAsShipCheckbox")
    private CheckBox shipToDifferentAddress;

    @FindBy(xpath = ".//div[@id='zPassUpdateWrapper']//input")
    private CheckBox updateMyAccountWithTheeseAddress;

    @FindBy(id = "addressUpdateLink")
    private Button billingContinue;

    public void enterAddress(String fName, String lName, String addr1, String addr2, String city, String state,
                             String zip, String country, String phone, boolean shipToDifferentAddr, boolean
                                     updateMyAccountAddr) {
        if (addr2 == null)
            addr2="";

        billingFirstName.sendKeys(fName);
        billingLastName.sendKeys(lName);
        billingAddress1.sendKeys(addr1);
        billingAddress2.sendKeys(addr2);
        billingCity.sendKeys(city);
        billingStateProvince.selectByValue(state);
        billingPostalCode.sendKeys(zip);
        billingCountry.selectByValue(country);
        billingPhone.sendKeys(phone);
        shipToDifferentAddress.set(shipToDifferentAddr);
        updateMyAccountWithTheeseAddress.set(updateMyAccountAddr);
        billingContinue.click();
    }
}
