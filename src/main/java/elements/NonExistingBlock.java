package elements;

import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

/**
 * Created by andriynavrotskyy on 12/26/16.
 */
@Name("non existing block")
@FindBy(id = "lol")
public class NonExistingBlock extends HtmlElement {
}
