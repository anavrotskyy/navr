package elements;

import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextInput;

@Name("Main Menu")
@FindBy(id = "masthead")
public class MainMenuElement extends HtmlElement {
    @FindBy(partialLinkText = "Knife Shop")
    private Link knifeShop;

    @FindBy(partialLinkText = "Build A Custom Knife")
    private Link buildCustomKnife;

    @FindBy(partialLinkText = "About Our Knives")
    private Link aboutOurKnives;

    @FindBy(partialLinkText = "About Buck")
    Link aboutBuck;

    @FindBy(id = "masthead-search-input")
    public TextInput searchField;

    @FindBy(id = "lolLOL")
    public TextInput nonExisting;

    @FindBy(id = "masthead-logo")
    public Link mainLogo;

    @FindBy(id = "nav-utility-login")
    public Link openLoginModal;

    public void setOpenLoginModal(){
        openLoginModal.click();
    }

    public void serchFor(String text) {
        searchField.clear();
        searchField.sendKeys(text);
        searchField.submit();
    }
}