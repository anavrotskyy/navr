import org.testng.Assert;
import org.testng.annotations.Test;

import pages.HomePage;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Created by andriynavrotskyy on 1/23/17.
 */
@Title("Testsuite #1 - about nothing")
public class YetAnotherTest extends BaseTest {

    private HomePage homePage;

    @Test
    public void elementIsNotThereTest() {
        homePage = new HomePage(driver).get();
        System.out.println(homePage.getTagFromExistingelement());
        System.out.println(homePage.getTagFromNONExistingelement());
        Assert.assertTrue(homePage.getTagFromExistingelement() != null);
        Assert.assertTrue(homePage.getTagFromNONExistingelement() != null);
    }

    @Test
    public void elementisDefinitelyThereTest() {
        homePage = new HomePage(driver).get();
        System.out.println(homePage.getTagFromExistingelement());
        System.out.println(homePage.getTagFromNONExistingelement());
    }

    @Test
    public void elementIsInTheMiddleTest() {
        homePage = new HomePage(driver).get();
        Assert.assertTrue(driver.getCurrentUrl().contains("buck"));
    }

    @Test
    public void somewhereAlongtheHighwayTest() {
        homePage = new HomePage(driver).get();
        Assert.assertTrue(driver.getCurrentUrl().contains("shpack"));
    }
}
