import org.testng.Assert;
import org.testng.annotations.Test;

import pages.CheckoutPage;
import pages.HomePage;
import pages.ProductDetailsPage;
import pages.SearchResultsPage;
import ru.yandex.qatools.allure.annotations.Title;

@Title("Testsuite #2 - aka main but definitely emty")
public class SimpleSearchTest extends BaseTest {

    private final static String knifeName = "Thorn";
    private final static String quantityTestString = "TOPS";
    private final static String email = "nickelstore@gmail.com";
    private final static String password = "Asd-12345";
    private final static String detailedProductName = "017 Thorn";

    private final static String name = "Andriy";
    private final static String surname = "Navrotskyy";
    private final static String addr1 = "bokee court 63";
    private final static String city = "Brooklyn";
    private final static String state = "New York";
    private final static String zip = "11663";
    private final static String country = "United States";
    private final static String phone = "5555555555";
    private final static int itemsQuantity = 2;
    private HomePage homePage;
    private SearchResultsPage searchResultsPage;
    private ProductDetailsPage productDetailsPage;

    @Test(enabled = true)
    public void simpleSearchTest() {
        HomePage homePage = new HomePage(driver).get();
        SearchResultsPage searchResultsPage = homePage.searchForKnife(knifeName);
        Assert.assertTrue(searchResultsPage.searchResultsElements.isProductFound(knifeName),
                "required product was not found or error occured");
    }

    @Test(enabled = true)
    public void searchResultsCorrectAmountTest() {
        HomePage homePage = new HomePage(driver).get();
        SearchResultsPage searchResultsPage = homePage.searchForKnife(quantityTestString);
        Assert.assertEquals(searchResultsPage.searchResultsElements.searchResults(), searchResultsPage.productCount()
                , "Search results quantity is wrong");
    }

    @Test(enabled = true)
    public void cartTest() {
        HomePage homePage = new HomePage(driver).get();
        //homePage.loginApplication(email,password);
        SearchResultsPage searchResultsPage = homePage.searchForKnife(knifeName);
        ProductDetailsPage productDetailsPage = searchResultsPage.selectProduct(detailedProductName);
        Assert.assertTrue(detailedProductName.equals(productDetailsPage.getProductName()), "something went wrong");
    }

    @Test(enabled = false)
    public void nestedLoadableComponentTest() {
        homePage = new HomePage(driver);
        searchResultsPage = homePage.searchForKnife(knifeName);
        productDetailsPage = searchResultsPage.selectProduct(knifeName);
        productDetailsPage.get();
        Assert.assertTrue(detailedProductName.equals(productDetailsPage.getProductName()), "something went wrong");
    }

    @Test(enabled = true)
    public void subtotalTest() {
        homePage = new HomePage(driver).get();
        searchResultsPage = homePage.searchForKnife(knifeName);
        productDetailsPage = searchResultsPage.selectProduct(knifeName);
        productDetailsPage.get();
        productDetailsPage.addToCart();
        productDetailsPage.openCartFrommodal();
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        checkoutPage.setItemsQuantity(itemsQuantity);
        Assert.assertEquals(checkoutPage.countExpectedSubtotal(itemsQuantity), checkoutPage.getActualSubtotal());
    }

    @Test(enabled = true)
    public void subtotalNegativeTest() {
        homePage = new HomePage(driver).get();
        searchResultsPage = homePage.searchForKnife(knifeName);
        productDetailsPage = searchResultsPage.selectProduct(knifeName);
        productDetailsPage.get();
        productDetailsPage.addToCart();
        productDetailsPage.openCartFrommodal();
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        checkoutPage.setItemsQuantity(itemsQuantity);
        Assert.assertEquals(checkoutPage.countExpectedSubtotal(400), checkoutPage.getActualSubtotal());
    }

    @Test(enabled = true)
    public void productNameTest() {
        homePage = new HomePage(driver).get();
        searchResultsPage = homePage.searchForKnife(knifeName);
        productDetailsPage = searchResultsPage.selectProduct(knifeName);
        productDetailsPage.get();
        productDetailsPage.addToCart();
        Assert.assertTrue(productDetailsPage.getProductName().contains("noshpa"), "Does not contain noshpa");
    }

    /**
     * Test for non existing element
     */
    @Test(enabled = true)
    public void elementWithoutBlockTest() {
        homePage = new HomePage(driver).get();
        System.out.println(homePage.getTagFromExistingelement());
        System.out.println(homePage.getTagFromNONExistingelement());
    }

    /**
     * Test for non existing block
     */
    @Test(enabled = false)
    public void blockTest() {
        homePage = new HomePage(driver).get();
        homePage.nONExistingBlock();
    }

    /**
     * Test for non existing element in existing block
     */
    @Test(enabled = false)
    public void elementInBlockTest() {
        homePage = new HomePage(driver).get();
        System.out.println(homePage.getTagFromExistingelement());
        homePage.getTagFromNONExistingElementInBlock();
    }
}